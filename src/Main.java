import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Book> books=new ArrayList<>();
        Scanner sin = new Scanner(System.in);

        String vv1="",vv2="";

        do{
            System.out.println("Введите:\n1 - Добавление книги\n2 - Поиск книги\n3 - Удаление книги\n0 - Выход");
            vv1=sin.next();
            switch (vv1){
                case "1":
                    books.add(new Book(books.size(),VvodS("Введите название книги: "),VvodS("Введите автора: "),VvodInt("Введите год издания: ")));
                    break;
                case "2":
                    ArrayList<Book> tempbook=new ArrayList<>();
                    do{
                        System.out.println("Введите:\n1 - Поиск по названию\n2 - Поиск по автору\n3 - Поиск по году издания\n0 - выход");
                        vv2=sin.next();
                        switch (vv2){
                            case "1":
                                tempbook=FindN(books,VvodS("Введите название книги:"));
                                if (tempbook.size()==0) System.out.println("Совпадений не найдено");
                                else Print(tempbook);
                                break;
                            case "2":
                                tempbook=FindA(books,VvodS("Введите автора книги:"));
                                if (tempbook.size()==0) System.out.println("Совпадений не найдено");
                                else Print(tempbook);
                                break;
                            case "3":
                                tempbook=FindY(books,VvodInt("Введите год издания книги:"));
                                if (tempbook.size()==0) System.out.println("Совпадений не найдено");
                                else Print(tempbook);
                                break;
                            case"0":break;
                                default:
                                    System.out.println("Неправильные данные");
                                    break;
                        }
                    }while (!vv2.equals("0"));
                    vv2="0";
                    break;
                case "3":
                    boolean exit=false;
                    int id=0;
                    do {
                        exit=false;
                        id=VvodInt("Введите ID книги");
                        if (id==-1) exit=true;
                        else {
                            tempbook=FindI(books,id);
                            if (tempbook.size()==0) System.out.println("Совпадений не найдено");
                            else {
                                books.remove(tempbook.get(0).id);
                                System.out.println("Книга удалена");
                                exit=true;
                            }
                        }
                    }while (!exit);
                    break;
                case "0":break;
                default:
                    System.out.println("Неправильные данные");
                    break;
            }
        }while (!vv1.equals("0"));
    }

    public static String VvodS(String msg) {
        Scanner sin = new Scanner(System.in);
        System.out.println(msg);
        return sin.next();
    }
    public static Integer VvodInt(String msg) {
        Scanner sin = new Scanner(System.in);
        System.out.println(msg);
        return sin.nextInt();
    }

    public static ArrayList<Book> FindN(ArrayList<Book> books,String name){
        ArrayList<Book> newbooks = new ArrayList<>();
        for (int i=0; i<books.size(); i++){
            if(books.get(i).name.equals(name)) newbooks.add(books.get(i));
        }
        return newbooks;
    }
    public static ArrayList<Book> FindA(ArrayList<Book> books,String author){
        ArrayList<Book> newbooks = new ArrayList<>();
        for (int i=0; i<books.size(); i++){
            if(books.get(i).author.equals(author)) newbooks.add(books.get(i));
        }
        return newbooks;
    }
    public static ArrayList<Book> FindY(ArrayList<Book> books,Integer year){
        ArrayList<Book> newbooks = new ArrayList<>();
        for (int i=0; i<books.size(); i++){
            if(books.get(i).year==year) newbooks.add(books.get(i));
        }
        return newbooks;
    }
    public static ArrayList<Book> FindI(ArrayList<Book> books,Integer id){
        ArrayList<Book> newbooks = new ArrayList<>();
        for (int i=0; i<books.size(); i++){
            if(books.get(i).id==id) newbooks.add(books.get(i));
        }
        return newbooks;
    }

    public static void Print(ArrayList<Book> books){
        for (int i=0; i<books.size(); i++)
        {
            System.out.println("ID: "+books.get(i).id+"\nНазвание: "+books.get(i).name+"\nАвтор: "+books.get(i).author+"\nГод издания: "+books.get(i).year+"\n");
        }
    }
}